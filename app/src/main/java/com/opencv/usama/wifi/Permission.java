package com.opencv.usama.wifi;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Permission extends AppCompatActivity {


//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case 1: {
//
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    Intent myIntent = new Intent(Permission.this, MainActivity.class);
//                   // myIntent.putExtra("key", value); //Optional parameters
//                    Permission.this.startActivity(myIntent);
//
//                    Toast.makeText(Permission.this, "Granted", Toast.LENGTH_LONG).show();
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    Toast.makeText(Permission.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }

    private TextView mainText;
    private ListView wifiDeviceList;
    private WifiManager mainWifi;
    private Permission.WifiReceiver receiverWifi;
    private List<ScanResult> wifiList;
    private StringBuilder sb;
    private String ssid_selected=null;
    private Context context=null;
Button connect_btn ;
    private static final int MY_PERMISSIONS_REQUEST =1 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

//        ActivityCompat.requestPermissions(Permission.this,
//                new String[]{
//                        Manifest.permission.ACCESS_WIFI_STATE,
//                        Manifest.permission.ACCESS_NETWORK_STATE,
//                        Manifest.permission.CHANGE_WIFI_STATE,
//                },
//                1);


        setTitle("Choose Switchbox");
        connect_btn =( Button) findViewById(R.id.connect_btn);

        wifiDeviceList=(ListView)findViewById(R.id.listView);
        mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!mainWifi.isWifiEnabled())
        {
            Toast.makeText(getApplicationContext(), "Turning WiFi ON...", Toast.LENGTH_LONG).show();
            mainWifi.setWifiEnabled(true);
        }


        wifiDeviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final String ssid = '"' + wifiList.get(position).SSID + '"';
                final String mac = wifiList.get(position).BSSID;
                String pass = "";


                LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                View promptView = inflater.inflate(R.layout.connect, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Permission.this);
                alertDialogBuilder.setView(promptView);
                final EditText ssid_et = (EditText) promptView.findViewById(R.id.editText_ssid);
                final EditText pass_et = (EditText) promptView.findViewById(R.id.editText_pswd);
                ssid_et.setText(wifiList.get(position).SSID);
                alertDialogBuilder
                        .setCancelable(true)
                        .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String ssid = '"' + wifiList.get(position).SSID + '"';
                                String password = '"' + pass_et.getText().toString() + '"';

                                System.out.println("ssid: "+ssid);
                                System.out.println("password: "+password);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    connectToHost(getApplicationContext(), ssid, password);
                                }
                                else{
                                    connectToHost2(getApplicationContext(), ssid, password);
                                }

                            }
                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .create()
                        .show();
            }
        });

    }


    public void connectToHost(Context context,String host,String password){
        mainWifi  = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wc=new WifiConfiguration();

        wc.SSID= host;
        wc.preSharedKey =  password;

        int netId=mainWifi.addNetwork(wc);


        try {
            mainWifi.enableNetwork(netId, true);
            mainWifi.setWifiEnabled(true);

            System.out.println("enabled network");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void connectToHost2(Context context,String host,String password){
        mainWifi  = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wc=new WifiConfiguration();

        wc.SSID= host;
        wc.preSharedKey =  password;
        wc.status = WifiConfiguration.Status.ENABLED;
        wc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA); // For WPA
        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For WPA2
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);


        int netId=mainWifi.addNetwork(wc);


        try {
            mainWifi.enableNetwork(netId, true);
            mainWifi.setWifiEnabled(true);

            System.out.println("enabled network");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }


    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            String action = intent.getAction();
            if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                sb = new StringBuilder();
                wifiList = mainWifi.getScanResults();
                Toast.makeText(c, "Broad " + wifiList.size(), Toast.LENGTH_SHORT).show();
                sb.append("\n Number Of Wifi connections :" + wifiList.size() + "\n\n");

                ArrayList<String> deviceList = new ArrayList<String>();
                for (int i = 0; i < wifiList.size(); i++) {
                    sb.append(new Integer(i + 1).toString() + ". ");
                    sb.append((wifiList.get(i)).toString());
                    sb.append("\n\n");
                    deviceList.add(wifiList.get(i).SSID);
                }

                ArrayAdapter arrayAdapter = new ArrayAdapter(Permission.this,
                        android.R.layout.simple_list_item_1, deviceList.toArray());

                wifiDeviceList.setAdapter(arrayAdapter);
            }

        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiverWifi);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        receiverWifi = new Permission.WifiReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(receiverWifi, intentFilter);
        getWifi();
    }



    private void getWifi() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Toast.makeText(Permission.this, "version>=marshmallow", Toast.LENGTH_SHORT).show();

            if (ContextCompat.checkSelfPermission(Permission.this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(Permission.this, "location turned off", Toast.LENGTH_SHORT).show();

                ActivityCompat.requestPermissions(Permission.this,new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, MY_PERMISSIONS_REQUEST);

            }

            else{
                Toast.makeText(Permission.this, "location turned on", Toast.LENGTH_SHORT).show();
                mainWifi.startScan();
            }

        }else {

            Toast.makeText(Permission.this, "scanning", Toast.LENGTH_SHORT).show();
            mainWifi.startScan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(Permission.this, "permission granted", Toast.LENGTH_SHORT).show();
                mainWifi.startScan();

            }else{
                Toast.makeText(Permission.this, "permission not granted", Toast.LENGTH_SHORT).show();
                return;
            }

        }
    }




}
